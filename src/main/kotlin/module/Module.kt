package module

import kotlin.math.roundToInt

class Module(val mass: Integer) {

    fun calculateFuel(): Int =
        (mass.toDouble() / 3).toInt() - 2

}
