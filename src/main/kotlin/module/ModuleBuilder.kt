package module

class ModuleBuilder {

    private lateinit var mass: Integer

    fun withMass(mass: Int): ModuleBuilder {
        this.mass = Integer(mass)
        return this
    }

    fun build(): Module = Module(mass)

    companion object {
        fun aModule(): ModuleBuilder {
            return ModuleBuilder()
        }
    }
}
