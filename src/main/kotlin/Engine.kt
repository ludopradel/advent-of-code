import module.Module

class Engine {
    private val modules: MutableList<Module> = mutableListOf()

    fun add(module: Module) {
        modules.add(module)
    }

    fun calculateFuel(): Int = modules.sumBy { module -> module.calculateFuel() }

}
