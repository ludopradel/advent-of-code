package module

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ModuleShould {

    @Test
    fun `calculate fuel needed based on mass 12`() {
        val module = ModuleBuilder.aModule().withMass(12).build()
        val fuelNeeded = module.calculateFuel()
        assertThat(fuelNeeded).isEqualTo(2)
    }

    @Test
    fun `calculate fuel needed based on mass 14`() {
        val module = ModuleBuilder.aModule().withMass(14).build()
        val fuelNeeded = module.calculateFuel()
        assertThat(fuelNeeded).isEqualTo(2)
    }


    @Test
    fun `calculate fuel needed based on mass 1969`() {
        val module = ModuleBuilder.aModule().withMass(1969).build()
        val fuelNeeded = module.calculateFuel()
        assertThat(fuelNeeded).isEqualTo(654)
    }

    @Test
    fun `calculate fuel needed based on mass 100756`() {
        val module = ModuleBuilder.aModule().withMass(100756).build()
        val fuelNeeded = module.calculateFuel()
        assertThat(fuelNeeded).isEqualTo(33583)
    }


}
